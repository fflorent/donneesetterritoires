resource "scaleway_object_bucket" "tmp_dumps" {
  provider = scaleway.project
  name     = "${var.project_slug}-tmp-dumps"
}
