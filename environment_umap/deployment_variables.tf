module "configure_umap_for_deployment" {
  source       = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version      = "0.1.0"
  repositories = [var.project_id]
  scope        = var.gitlab_environment_scope

  cluster_user           = module.namespace.user
  cluster_token          = module.namespace.token
  cluster_namespace      = module.namespace.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)

  base-domain = var.base_domain
}

resource "random_password" "umap_secret_key" {
  length  = 64
  special = false
}
resource "gitlab_project_variable" "umap_helm_values" {
  project           = var.project_id
  environment_scope = var.gitlab_environment_scope
  key               = "HELM_UPGRADE_VALUES"
  value             = <<-EOT
    ingress:
      enabled: true
      annotations:
        kubernetes.io/ingress.class: haproxy
        acme.cert-manager.io/http01-edit-in-place: "true"
        haproxy.org/response-set-header: Referrer-Policy same-origin
      hosts:
        - host: ${var.base_domain}
          tls: true
    envVars:
      DATABASE_URL: "postgis://${urlencode(module.postgresql.user)}:${urlencode(module.postgresql.password)}@${module.postgresql.host}/${module.postgresql.dbname}"
      SITE_URL: "https://${var.base_domain}"
      UMAP_ALLOW_ANONYMOUS: False
      UMAP_USE_UNACCENT: True
      SECRET_KEY: ${random_password.umap_secret_key.result}
      ALLOWED_HOSTS: "*"
      SOCIAL_AUTH_MONCOMPTEPRO_OIDC_ENDPOINT: ${var.sso_endpoint}
      SOCIAL_AUTH_MONCOMPTEPRO_KEY: ${var.sso_key}
      SOCIAL_AUTH_MONCOMPTEPRO_SECRET: ${var.sso_secret}
    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}
    resources:
      requests:
        cpu: 100m
      limits:
        memory: 1Gi
    service:
      targetPort: 8000
    persistence:
      uploads:
        size: 10Gi
        mountPath: /srv/umap/uploads
    affinity:
      podAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
        - labelSelector:
            matchExpressions:
            - key: app.kubernetes.io/instance
              operator: In
              values:
              - umap
            - key: app.kubernetes.io/name
              operator: In
              values:
              - webapp
          topologyKey: kubernetes.io/hostname
    EOT
  variable_type     = "file"
}

resource "gitlab_project_variable" "csrf_trusted_origins" {
  project           = var.project_id
  environment_scope = var.gitlab_environment_scope
  key               = "HELM_ENV_VAR_CSRF_TRUSTED_ORIGINS"
  value             = join(",", ["https://${var.base_domain}"])
}
