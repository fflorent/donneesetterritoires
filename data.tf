data "scaleway_k8s_cluster" "prod" {
  provider   = scaleway.production
  cluster_id = var.scaleway_cluster_production_cluster_id
}

data "scaleway_k8s_cluster" "dev" {
  provider   = scaleway.development
  cluster_id = var.scaleway_cluster_development_cluster_id
}

data "gitlab_project" "zonage" {
  id = 37787354
}

data "gitlab_project" "grist_form" {
  id = 46093914
}

data "gitlab_project" "umap" {
  id = 47583365
}

data "gitlab_project" "catalog" {
  id = 47799095
}

locals {
  gitlab_project_ids = {
    zonage     = data.gitlab_project.zonage.id
    grist_form = data.gitlab_project.grist_form.id
    umap       = data.gitlab_project.umap.id
    catalog    = data.gitlab_project.catalog.id
  }
}

