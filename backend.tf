module "repo_config_zonage" {
  source  = "gitlab.com/vigigloo/tf-modules-gitlab/projectcommonconfiguration"
  version = "0.0.1"

  project_id = data.gitlab_project.zonage.id
}

moved {
  from = module.repo_config_backend.gitlab_deploy_token.deploy-token
  to   = module.repo_config_zonage.gitlab_deploy_token.deploy-token
}
moved {
  from = module.repo_config_backend.gitlab_project_variable.deploy-registry
  to   = module.repo_config_zonage.gitlab_project_variable.deploy-registry
}
moved {
  from = module.repo_config_backend.gitlab_project_variable.deploy-token
  to   = module.repo_config_zonage.gitlab_project_variable.deploy-token
}
moved {
  from = module.repo_config_backend.gitlab_project_variable.deploy-username
  to   = module.repo_config_zonage.gitlab_project_variable.deploy-username
}
