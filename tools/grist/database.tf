resource "scaleway_object_bucket" "db_backups" {
  name = "${var.project_slug}-db-backups"
}

module "postgresql" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.22"

  chart_name = "postgresql"
  namespace  = module.namespace.namespace

  pg_volume_size = var.database_volume_size
  pg_replicas    = 2

  pg_backups_volume_enabled             = true
  pg_backups_volume_size                = "40Gi"
  pg_backups_volume_full_schedule       = "45 2 * * 0"
  pg_backups_volume_incr_schedule       = "45 2 * * 1-6"
  pg_backups_volume_full_retention      = 4
  pg_backups_volume_full_retention_type = "count"

  pg_backups_s3_enabled       = true
  pg_backups_s3_bucket        = scaleway_object_bucket.db_backups.name
  pg_backups_s3_region        = "fr-par"
  pg_backups_s3_endpoint      = "s3.fr-par.scw.cloud"
  pg_backups_s3_access_key    = var.scaleway_project_config.access_key
  pg_backups_s3_secret_key    = var.scaleway_project_config.secret_key
  pg_backups_s3_full_schedule = "45 3 * * 0"
  pg_backups_s3_incr_schedule = "45 3 * * 1-6"
  values = [
    <<-EOT
    postgresVersion: 14
    imagePgBackRest: null
    imagePostgres: null
    patroni:
      dynamicConfiguration:
        postgresql:
          parameters:
            log_connections: on
            log_disconnections: on
            log_duration: on
            logging_collector: off
            log_statement: all
    EOT
    ,
    file("${path.module}/db_resources.yaml"),
    <<-EOT
    podAnnotations:
      monitoring-org-id: "${var.monitoring_org_id}"
    EOT
  ]
}
