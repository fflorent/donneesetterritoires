module "kubeconfig" {
  for_each = var.generate_kubeconfigs
  source   = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version  = "0.3.1"

  filename               = "${var.project_slug}-${each.key}.yml"
  namespace              = module.namespace.namespace
  username               = each.key
  project_name           = var.project_slug
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
