variable "project_slug" {
  type = string
}
variable "override_namespace" {
  type    = string
  default = null
}

variable "domain" {
  type = string
}

variable "scaleway_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
  sensitive = true
}

variable "oauth_domain" {
  type = string
}

variable "oauth_client_id" {
  type = string
}

variable "oauth_client_secret" {
  type      = string
  sensitive = true
}
variable "oauth_scopes" {
  type    = string
  default = "openid email profile organization"
}

variable "default_email" {
  type = string
}

variable "monitoring_org_id" {
  type      = string
  sensitive = true
}

variable "kubeconfig" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
  default = null
}

variable "generate_kubeconfigs" {
  type    = set(string)
  default = []
}

variable "grist_doc_wk_replicas" {
  type = number
}
variable "grist_doc_wk_requests_cpu_m" {
  type = number
}
variable "grist_doc_wk_limits_memory_mb" {
  type = number
}
variable "grist_home_wk_replicas" {
  type = number
}
variable "grist_home_wk_requests_cpu_m" {
  type = number
}
variable "grist_home_wk_limits_memory_mb" {
  type = number
}
locals {
  grist_doc_wk_total_limits_memory_mb  = var.grist_doc_wk_replicas * var.grist_doc_wk_limits_memory_mb
  grist_home_wk_total_limits_memory_mb = var.grist_home_wk_replicas * var.grist_home_wk_limits_memory_mb
  grist_doc_wk_total_requests_cpu_m    = var.grist_doc_wk_replicas * var.grist_doc_wk_requests_cpu_m
  grist_home_wk_total_requests_cpu_m   = var.grist_home_wk_replicas * var.grist_home_wk_requests_cpu_m
  namespace_headroom_limits_memory_mb  = 12 * 1024
  namespace_headroom_requests_cpu_m    = 4 * 1000
  namespace_limits_memory              = "${local.grist_doc_wk_total_limits_memory_mb + local.grist_home_wk_total_limits_memory_mb + local.namespace_headroom_limits_memory_mb}Mi"
  namespace_requests_cpu               = "${local.grist_doc_wk_total_requests_cpu_m + local.grist_home_wk_total_requests_cpu_m + local.namespace_headroom_requests_cpu_m}m"

  grist_home_wk_container_limits_memory = "${var.grist_home_wk_limits_memory_mb}Mi"
  grist_doc_wk_container_limits_memory  = "${var.grist_doc_wk_limits_memory_mb}Mi"
  grist_home_wk_container_requests_cpu  = "${var.grist_home_wk_requests_cpu_m}m"
  grist_doc_wk_container_requests_cpu   = "${var.grist_doc_wk_requests_cpu_m}m"
}

variable "grist_extra_env" {
  type    = map(string)
  default = {}
}
variable "cors_allow_origin" {
  type = string
}

variable "image_repository" {
  type = string
}
variable "image_tag" {
  type = string
}

variable "database_volume_size" {
  type    = string
  default = "10Gi"
}

variable "grist_limits_memory_mb" {
  type        = string
  default     = "deprecated"
  description = "Deprecated, only kept to easily compare across modules"
}

variable "custom_script" {
  type = object({
    path    = string
    content = string
  })
  default = null
}
